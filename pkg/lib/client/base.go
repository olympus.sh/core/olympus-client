package client

import (
	"net/http"
	"time"
)

const (
	BaseURLV1 = "https://olympus.sh/v1"
)

type Client struct {
	BaseURL    string
	apiKey     string
	HTTPClient *http.Client
}

type errorResponse struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

type successResponse struct {
	Code int         `json:"code"`
	Data interface{} `json:"data"`
}

func newClient(url string, apiKey string, timeout int64) *Client {
	return &Client{
		BaseURL: url,
		apiKey:  apiKey,
		HTTPClient: &http.Client{
			Timeout: time.Duration(timeout),
		},
	}
}
// NewV1Client Provides a V1 client for the Version 1 of the API, with 60 seconds timeout. An
// API Key must be provided as the ID of the client.
func NewV1Client(apiKey string) *Client {
	return newClient(BaseURLV1, apiKey, time.Minute.Seconds())
}